<%-- 
    Document   : lista
    Created on : 11-05-2021, 21:01:09
    Author     : Paty
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="root.entity.Cuentauno"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Cuentauno> cuentas = (List<Cuentauno>) request.getAttribute("listacuenta");
    Iterator<Cuentauno> itcuentas = cuentas.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <form  name="form" action="controlador" method="POST">
            <div class="cover-container d-flex h-100 p-3 mx-auto flex-column">

                <table border="1">
                    <thead>
                    <th>Codigo </th>
                    <th>Descripcion </th>

                    <th>Usuario</th>
                    <th>Contraseña </th>

                    <th>Notas </th>
                    </thead>
                    </tbody>
                    <%while (itcuentas.hasNext()) {
                            Cuentauno c1 = itcuentas.next();%>
                    <tr>
                        <td><%= c1.getCodigo()%></td>
                        <td><%= c1.getDescripcion()%></td>
                        <td><%= c1.getUsuario()%></td>
                        <td><%= c1.getContraseña()%></td>
                        <td><%= c1.getNotas()%></td>
                        <td> <input type="radio" name="seleccion" value="<%= c1.getCodigo()%>"> </td>
                    </tr>
                    <%}%>                
                    </tbody> 

                </table>
                <button type="submit" name="accion"  style="background-color:palegreen"  value="eliminar" class="btn btn-success">Eliminar cuenta</button>
                <button type="submit" name="accion" style="background-color:palegreen"  value="Consultar" class="btn btn-success">Consultar cuenta</button>  
        </form>   

    </body>
</html>