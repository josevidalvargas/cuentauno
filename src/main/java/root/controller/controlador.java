/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import root.dao.CuentaunoJpaController;
import root.dao.exceptions.NonexistentEntityException;
import root.entity.Cuentauno;

/**
 *
 * @author Paty
 */
@WebServlet(name = "controlador", urlPatterns = {"/controlador"})
public class controlador extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet controlador</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet controlador at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

      
        System.out.println("doPost");
        String accion = request.getParameter("accion");

        if (accion.equals("ingreso")) {

            request.getRequestDispatcher("registro.jsp").forward(request, response);

        }

        if (accion.equals("nuevacuenta")) {

            try {

                String codigo = request.getParameter("code");
                String descripcion = request.getParameter("descripcion");
                String usuario = request.getParameter("usuario");
                String contraseña = request.getParameter("contrasena");
                String notas = request.getParameter("notas");

                Cuentauno cuenta = new Cuentauno();

                cuenta.setCodigo(codigo);
                cuenta.setDescripcion(descripcion);
                cuenta.setUsuario(usuario);
                cuenta.setContraseña(contraseña);
                cuenta.setNotas(notas);

                CuentaunoJpaController dao = new CuentaunoJpaController();
                dao.create(cuenta);
            } catch (Exception ex) {
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            }
          
            
        }
        if (accion.equals("verlista")) {

            CuentaunoJpaController dao = new CuentaunoJpaController();

            List<Cuentauno> lista = dao.findCuentaunoEntities();
            request.setAttribute("listacuenta", lista);
            request.getRequestDispatcher("lista.jsp").forward(request, response);

        }

        if (accion.equals("eliminar")) {
            try {
                String codigo = request.getParameter("seleccion");

                CuentaunoJpaController dao = new CuentaunoJpaController();
                dao.destroy(codigo);
            } catch (NonexistentEntityException ex) {
                Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        if (accion.equals("Consultar")) {
            String Codigo = request.getParameter("seleccion");
            CuentaunoJpaController dao = new CuentaunoJpaController();
            Cuentauno cuenta = dao.findCuentauno(Codigo);
            request.setAttribute("consultas", cuenta);
            request.getRequestDispatcher("consulta.jsp").forward(request, response);

         }    
        if (accion.equals("editar")) {
           try {

                String codigo = request.getParameter("code");
                String descripcion = request.getParameter("descripcion");
                String usuario = request.getParameter("usuario");
                String contraseña = request.getParameter("contrasena");
                String notas = request.getParameter("notas");

                Cuentauno cuenta = new Cuentauno();

                cuenta.setCodigo(codigo);
                cuenta.setDescripcion(descripcion);
                cuenta.setUsuario(usuario);
                cuenta.setContraseña(contraseña);
                cuenta.setNotas(notas);

                CuentaunoJpaController dao = new CuentaunoJpaController();

          
                
                dao.edit(cuenta);
                
                  List<Cuentauno> lista = dao.findCuentaunoEntities();
            request.setAttribute("listacuenta", lista);
            request.getRequestDispatcher("lista.jsp").forward(request, response);
                
           } catch (Exception ex) {
         Logger.getLogger(controlador.class.getName()).log(Level.SEVERE, null, ex);
           
           }
              }
         processRequest(request, response);

    }
    
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
