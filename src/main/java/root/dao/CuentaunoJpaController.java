/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.dao;

import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Persistence;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import root.dao.exceptions.NonexistentEntityException;
import root.dao.exceptions.PreexistingEntityException;
import root.entity.Cuentauno;

/**
 *
 * @author Paty
 */
public class CuentaunoJpaController implements Serializable {

    public CuentaunoJpaController() {
        
    }
    private EntityManagerFactory emf = Persistence.createEntityManagerFactory("my_persistence_unit");

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public void create(Cuentauno cuentauno) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(cuentauno);
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findCuentauno(cuentauno.getCodigo()) != null) {
                throw new PreexistingEntityException("Cuentauno " + cuentauno + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Cuentauno cuentauno) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            cuentauno = em.merge(cuentauno);
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                String id = cuentauno.getCodigo();
                if (findCuentauno(id) == null) {
                    throw new NonexistentEntityException("The cuentauno with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(String id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Cuentauno cuentauno;
            try {
                cuentauno = em.getReference(Cuentauno.class, id);
                cuentauno.getCodigo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The cuentauno with id " + id + " no longer exists.", enfe);
            }
            em.remove(cuentauno);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Cuentauno> findCuentaunoEntities() {
        return findCuentaunoEntities(true, -1, -1);
    }

    public List<Cuentauno> findCuentaunoEntities(int maxResults, int firstResult) {
        return findCuentaunoEntities(false, maxResults, firstResult);
    }

    private List<Cuentauno> findCuentaunoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Cuentauno.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Cuentauno findCuentauno(String id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Cuentauno.class, id);
        } finally {
            em.close();
        }
    }

    public int getCuentaunoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Cuentauno> rt = cq.from(Cuentauno.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
