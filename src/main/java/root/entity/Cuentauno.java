/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Paty
 */
@Entity
@Table(name = "cuentauno")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Cuentauno.findAll", query = "SELECT c FROM Cuentauno c"),
    @NamedQuery(name = "Cuentauno.findByCodigo", query = "SELECT c FROM Cuentauno c WHERE c.codigo = :codigo"),
    @NamedQuery(name = "Cuentauno.findByDescripcion", query = "SELECT c FROM Cuentauno c WHERE c.descripcion = :descripcion"),
    @NamedQuery(name = "Cuentauno.findByUsuario", query = "SELECT c FROM Cuentauno c WHERE c.usuario = :usuario"),
    @NamedQuery(name = "Cuentauno.findByContrase\u00f1a", query = "SELECT c FROM Cuentauno c WHERE c.contrase\u00f1a = :contrase\u00f1a"),
    @NamedQuery(name = "Cuentauno.findByNotas", query = "SELECT c FROM Cuentauno c WHERE c.notas = :notas")})
public class Cuentauno implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 2147483647)
    @Column(name = "codigo")
    private String codigo;
    @Size(max = 2147483647)
    @Column(name = "descripcion")
    private String descripcion;
    @Size(max = 2147483647)
    @Column(name = "usuario")
    private String usuario;
    @Size(max = 2147483647)
    @Column(name = "contrase\u00f1a")
    private String contraseña;
    @Size(max = 2147483647)
    @Column(name = "notas")
    private String notas;

    public Cuentauno() {
    }

    public Cuentauno(String codigo) {
        this.codigo = codigo;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getUsuario() {
        return usuario;
    }

    public void setUsuario(String usuario) {
        this.usuario = usuario;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

    public String getNotas() {
        return notas;
    }

    public void setNotas(String notas) {
        this.notas = notas;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigo != null ? codigo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Cuentauno)) {
            return false;
        }
        Cuentauno other = (Cuentauno) object;
        if ((this.codigo == null && other.codigo != null) || (this.codigo != null && !this.codigo.equals(other.codigo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "root.entity.Cuentauno[ codigo=" + codigo + " ]";
    }
    
}
